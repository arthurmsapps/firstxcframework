 Pod::Spec.new do |s|
  s.name          = "ArthurTestFramwork"
  s.version       = "1.0.6"
  s.summary       = "CFramework that print Arthur for tests"
  s.homepage     = "https://github.com/ArthurMsApps/FirstXCFramework"
  s.authors      = { 'Arthur Zakharov' => 'arthur@msapps.mobi' }
  s.license      = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2018
                   Permission is granted to...
                  LICENSE
                }

  s.source       = { :git => 'https://ArthurMsApps@bitbucket.org/arthurmsapps/firstxcframework.git', :tag => s.version.to_s }
  s.ios.vendored_frameworks = "XCFramework/xcframeworks/ArthurTestFramwork.xcframework"
  s.platform = :ios
  s.swift_version = "5.0"
  s.ios.deployment_target  = '15.4'
end